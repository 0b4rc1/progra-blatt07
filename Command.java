public sealed abstract class Command permits ListFiles {

    VCS vcs;

    public Command(VCS vcs) {
      this.vcs = vcs;
    }

    public void execute() {
        // override me!
    }

    public static Command parse(String cmdName, VCS vcs) {
      if (cmdName.equals("exit")) return new Exit(vcs);
      if (cmdName.equals("listfiles")) return new ListFiles(vcs);
      if (cmdName.equals("commit")) return new Commit(vcs);

      System.out.println("no such command: " + cmdName);
      return null;
    }
}

final class Exit extends ListFiles {

  public Exit(VCS vcs) {
    super(vcs);
  }
  public void execute() {
    Util.exit();
  }
}

non-sealed class ListFiles extends Command {
  public ListFiles(VCS vcs) {
    super(vcs);
  }

  public void execute() {
    String[] files = Util.listFiles(vcs.getRootDir());
    for (String file : files) {
      System.out.println(file);
    }
  }
}


interface Modifying {
  String getInformation();
}

class Commit extends ListFiles implements Modifying {
  public Commit(VCS vcs) {
    super(vcs);
  }

  public String getInformation() {
    return """
    This command does the following modifying operations:
    Files: Copy and Move
    Directory: create""";
  }

  public void execute() {
    String new_timestamp_dir = Util.appendFileOrDirname(vcs.getBackupDir(), Util.getTimestamp());
    Util.mkdir(new_timestamp_dir);

    String[] old_files = Util.listFiles(vcs.getBackupDir());
    for (String old_fname : old_files) {
      String old_fpath = Util.appendFileOrDirname(vcs.getBackupDir(), old_fname);
      String new_fpath = Util.appendFileOrDirname(new_timestamp_dir, old_fname);
      Util.moveFile(old_fpath, new_fpath);
    }

    String[] files = Util.listFiles(vcs.getRootDir());
    for (String fname: files) {
      String old_fpath = Util.appendFileOrDirname(vcs.getRootDir(), fname);
      String new_fpath = Util.appendFileOrDirname(vcs.getBackupDir(), fname);
      Util.copyFile(old_fpath, new_fpath);
    }

    System.out.println("Committed the following files:");
    super.execute();
  }
}
